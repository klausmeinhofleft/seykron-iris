# Iris

![Iris](iris.png)

[Iris](https://telegram.me/Ir1sBot) es la mensajera de R'lyeh, es los ojos de Cthulhu en el ciberespacio.

Desde la superficie hasta las profundidades del Polo de Inaccesibilidad, [Iris](https://telegram.me/Ir1sBot)
recibe y comunica todo lo que pasa en R'lyeh; es el único ser que puede entrar y salir del sueño de Cthulhu sin
perder la cordura.

Por eso la invocamos para que avise en el ciberespacio cuando hay novedades importantes en R'lyeh.
[Iris](https://telegram.me/Ir1sBot) nos avisará por Telegram cuando ocurran los siguientes eventos:

* Si se cae algún servicio de R'lyeh.
* Cuando haya un mensaje importante de la comunidad de R'lyeh.

A [Iris](https://telegram.me/Ir1sBot) hay que hablarle e invocarla de las siguientes maneras:

[/list_sources](src/main/kotlin/be/rlab/iris/domain/command/ListSources.kt) para que nos muestre la lista
de fuentes a las que podemos subscribirnos para esperar mensajes.

[/subscribe](src/main/kotlin/be/rlab/iris/domain/command/Subscribe.kt) para subscribirnos a alguna de las
fuentes de mensajes. Si la invocamos de esta forma en un grupo o canal, [Iris](https://telegram.me/Ir1sBot) nos
va a subscribir a los mensajes que envie el grupo o canal.

[/enable_notifications](src/main/kotlin/be/rlab/iris/domain/command/EnableNotifications.kt) para
activar las notificaciones de nuevos mensajes en un grupo o canal. Con este pedido debemos decirle
a [Iris](https://telegram.me/Ir1sBot) qué fuentes de mensajes debe observar para notificar mensajes al grupo
o canal.

[/inbox_read](src/main/kotlin/be/rlab/iris/domain/command/InboxRead.kt) para leer mensajes pendientes.
Si le pedimos esto a [Iris](https://telegram.me/Ir1sBot) en la soledad de nuestros mensajes privados, nos
leerá los mensajes destinados a nosotras. Si le pedimos que lea los mensajes en un grupo, nos leerá
los mensajes destinados a la comunidad.

[/inbox_count](src/main/kotlin/be/rlab/iris/domain/command/InboxCount.kt) para pedirle a
[Iris](https://telegram.me/Ir1sBot) que nos cuente cuántos mensajes tenemos pendientes y de qué fuentes.

## Pedirle a Iris que envie mensajes

Para enviar mensajes a todas las personas subscriptas a Iris, se debe utilizar el siguiente
[servicio web](src/main/kotlin/be/rlab/iris/application/PushEventController.kt):

```
Endpoint: http://localhost:8090/eoc/notifications
Método: POST
Headers:
  Content-Type: application/json
Body:
  {
    "user_name": "cthulhu",
    "subject": "test notification",
    "summary": "service is down",
    "event_type":"SERVICE_STATUS",
    "additional_data": {
      "service_host": "https://nonexistent.url"
    }
  }
```

Este ejemplo con curl sería de la siguiente manera:

```
curl -i -XPOST \
  -H'Content-Type: application/json' \
  -d'{"user_name":"cthulhu","subject":"test notification","summary": "a summary","event_type":"SERVICE_STATUS","additional_data":{"service_host":"https://nonexistent.url"}}' \
  http://localhost:8090/eoc/notifications
```

## Desarrollo

[Iris](https://telegram.me/Ir1sBot) es un bot de Telegram basado en [Tehanu](https://git.rlab.be/seykron/tehanu).
Recibe [notificaciones](src/main/kotlin/be/rlab/iris/domain/model/Notification.kt) a través de un
[servicio web](src/main/kotlin/be/rlab/iris/application/PushEventController.kt) y envía el mensaje a todas las
personas y grupos [subscriptos](src/main/kotlin/be/rlab/iris/domain/model/Subscription.kt).

### Configuración

La [configuración](src/main/resources/application.conf) está manejada con
[typesafe](https://github.com/lightbend/config) y contiene:

* Configuración del bot
* Configuración del data source

La configuración del bot incluye permisos, usuario, y token de la API de Telegram. La configuración del
data source contiene información para conectarse a la base de datos.


### Base de datos

Se puede configurar cualquier data source [soportado por Exposed](https://github.com/JetBrains/Exposed#dialects). De
manera predeterminada usa una base de datos H2 configurada en modo servidor. La configuración predeterminada es
la siguiente:

```
db {
  url: "jdbc:h2:tcp://localhost:7777/tmp/h2-data-iris"
  user: "sa"
  password: ""
  driver: "org.h2.Driver"
  log-statements: false
}
```

La configuración de docker-compose crea un contenedor con una base de datos H2 en modo servidor y
lo ejecuta escuchando en el puerto 7777. Para no utilizar docker/docker-compose se puede cambiar la url
del data source para usar una base de datos H2 embebida:

```
  url: "jdbc:h2:file:./db/default"
```

### Ejecución

Para ejecutar la aplicación en desarrollo, se deben definir las siguientes variables de entorno:

*bot_handle*: nombre de usuario del bot. Por ejemplo: @Ir1sBot
*bot_access_token*: token de la API de Telegram.
*bot_default_admin*: id del usuario de telegram que tiene permisos de root en el bot.

Una vez definidas estas variables de entorno, hay que ejecutar el punto de entrada que es el siguiente:

```
be.rlab.iris.Application
```

Con esto ya quedaría corriendo la aplicación en el puerto 8090 y se pueden enviar comandos desde Telegram
al usuario del bot.
