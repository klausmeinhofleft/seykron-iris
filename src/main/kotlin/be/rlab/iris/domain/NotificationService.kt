package be.rlab.iris.domain

import be.rlab.iris.domain.MemorySlots.CHANNELS_SUBSCRIPTIONS
import be.rlab.iris.domain.MemorySlots.NOTIFICATIONS
import be.rlab.iris.domain.model.Content
import be.rlab.iris.domain.model.DefaultSources
import be.rlab.iris.domain.model.Flag
import be.rlab.iris.domain.model.Notification
import be.rlab.tehanu.domain.BotAware
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.Tehanu
import be.rlab.tehanu.util.ObjectMapperFactory
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

class NotificationService(
    memory: Memory,
    private val inboxService: InboxService
): BotAware {

    companion object {
        private const val POLLING_DELAY: Long = 3000
    }

    private var notifications: List<Notification> by memory.slot(NOTIFICATIONS, listOf<Notification>())
    private var channelSubscriptions: Map<UUID, List<DefaultSources>> by memory.slot(CHANNELS_SUBSCRIPTIONS, mapOf<UUID, List<DefaultSources>>())
    override lateinit var tehanu: Tehanu

    private val notificationHandlers: Map<DefaultSources, (Notification) -> Unit> = mapOf(
        DefaultSources.SERVICE_STATUS to this::serviceStatus
    )

    fun push(notification: Notification) {
        notifications = notifications + notification
    }

    fun poll() = GlobalScope.launch {
        while (true) {
            delay(POLLING_DELAY)

            notifications = notifications.filter { notification ->
                notification.flags.contains(Flag.UNSEEN)
            }.map { notification ->
                val defaultSource: DefaultSources = DefaultSources.fromEventType(notification.eventType)
                val handler: (Notification) -> Unit = notificationHandlers.getOrDefault(
                    defaultSource, this@NotificationService::broadcast
                )
                handler(notification)
                notification.markAsSeen()
            }
        }
    }

    private fun broadcast(notification: Notification) {
        val defaultSource: DefaultSources = DefaultSources.fromEventType(notification.eventType)

        if (defaultSource != DefaultSources.UNKNOWN) {
            inboxService.broadcast(
                source = defaultSource.toSource(),
                from = notification.userName,
                subject = notification.subject,
                content = Content.yaml(
                    ObjectMapperFactory.yamlMapper.writeValueAsString(notification.additionalData)
                ),
                timestamp = notification.timestamp
            )
            inboxService.listSubscriptors(defaultSource.toSource()).forEach { subscriptor ->
                tehanu.sendMessage(subscriptor, "Tenés un mensaje nuevo desde: $defaultSource")
            }
        }
    }

    private fun serviceStatus(notification: Notification) {
        channelSubscriptions.filterValues { subscriptions ->
            subscriptions.contains(DefaultSources.SERVICE_STATUS)
        }.forEach { (chatId, _) ->
            val summary: String = notification.summary ?: notification.additionalData.entries.joinToString("; ")
            tehanu.sendMessage(chatId, "Servicios caidos: $summary")
        }

        broadcast(notification)
    }

}
