package be.rlab.iris.domain

object MemorySlots {
    const val MESSAGES_SLOT: String = "messages"
    const val INBOXES_SLOT: String = "inboxes"
    const val SUBSCRIPTIONS_SLOT: String = "subscriptions"
    const val CONTENT_SLOT: String = "content"
    const val NOTIFICATIONS: String = "notifications"
    const val CHANNELS_SUBSCRIPTIONS: String = "channels_subscriptions"
}
