package be.rlab.iris.domain

import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.model.User
import be.rlab.iris.domain.MemorySlots.INBOXES_SLOT
import be.rlab.iris.domain.MemorySlots.MESSAGES_SLOT
import be.rlab.iris.domain.MemorySlots.SUBSCRIPTIONS_SLOT
import be.rlab.iris.domain.model.*
import org.joda.time.DateTime
import java.util.*

class InboxService(
    memory: Memory,
    private val accessControl: AccessControl,
    private val contentService: ContentService
) {

    companion object {
        private const val INBOX_TAG: String = "INBOX"
    }

    private var messages: List<Message> by memory.slot(MESSAGES_SLOT, listOf<Message>())
    private var inboxes: List<Inbox> by memory.slot(INBOXES_SLOT, listOf<Inbox>())
    private var subscriptions: Map<Long, List<Subscription>> by memory.slot(
        SUBSCRIPTIONS_SLOT,
        mapOf<Long, List<Subscription>>()
    )

    fun listMessages(
        userId: Long
    ): List<Message> {
        return listMessages(userId) { _, messages ->
            messages
        }.flatten()
    }

    fun<T> listMessages(
        userId: Long,
        callback: (Inbox, List<Message>) -> T
    ): List<T> {
        return subscriptions[userId]?.map { subscription ->
            val inbox: Inbox = inboxes.find { inbox ->
                inbox.id == subscription.inboxId
            } ?: throw RuntimeException("Inbox not found for subscription: $subscription")

            callback(inbox, listMessages(subscription.inboxId))
        } ?: emptyList()
    }

    fun listSubscriptors(source: Source): List<User> {
        return subscriptions.filterValues { subscriptions ->
            subscriptions.any { subscription ->
                subscription.source == source
            }
        }.map { (userId, _) ->
            accessControl.findUserById(userId)
        }.requireNoNulls()
    }

    fun getMessage(
        userId: Long,
        index: Int
    ): Message? {
        return listMessages(userId).find { message ->
            message.index == index
        }
    }

    fun readContent(message: Message): Content {
        return contentService.retrieve(message.contentId)
            ?: throw RuntimeException("Content for message ${message.messageId} not found")
    }

    fun count(userId: Long): Map<Source, Int> {
        return subscriptions[userId]?.map { subscription ->
            subscription.source to listMessages(subscription.inboxId).size
        }?.toMap() ?: emptyMap()
    }

    fun isSubscribed(
        userId: Long,
        source: Source
    ): Boolean {
        return subscriptions[userId]?.any { subscription ->
            subscription.source == source
        } ?: false
    }

    fun subscribe(
        userId: Long,
        source: Source
    ) {
        val exists: Boolean = subscriptions[userId]?.any { subscription ->
            subscription.source == source
        } ?: false

        if (!exists) {
            val inbox: Inbox = resolveInbox(userId, source, Inbox.new(
                userId = userId,
                source = source
            ))
            val newSubscriptions: List<Subscription> = subscriptions.getOrDefault(userId, emptyList()) +
                Subscription.new(
                    inboxId = inbox.id,
                    source = source
                )
            subscriptions = subscriptions + (userId to newSubscriptions)
        }
    }

    fun unsubscribe(
        userId: Long,
        source: Source
    ) {
        val inbox: Inbox = resolveInbox(userId, source)
        val currentSubscriptions: List<Subscription> = subscriptions.getValue(userId)
        val newSubscriptions: List<Subscription> = currentSubscriptions.filter { subscription ->
            subscription.inboxId != inbox.id &&
            subscription.source != source
        }

        subscriptions = subscriptions + (userId to newSubscriptions)
    }

    fun broadcast(
        chatId: UUID,
        from: String,
        subject: String,
        content: Content,
        timestamp: DateTime
    ) {
        val contentId: Long = contentService.store(content)
        val source: Source = Source.from(accessControl.findChatById(chatId)
            ?: throw RuntimeException("Chat $chatId not found"))

        accessControl.listUsers(chatId).forEach { user ->
            notify(
                userId = user.id,
                source = source,
                from = from,
                subject = subject,
                contentId = contentId,
                timestamp = timestamp
            )
        }
    }

    fun broadcast(
        source: Source,
        from: String,
        subject: String,
        content: Content,
        timestamp: DateTime
    ) {
        val contentId: Long = contentService.store(content)

        subscriptions.filterValues { subscriptions ->
            subscriptions.any { subscription ->
                subscription.source == source
            }
        }.forEach { (userId, _) ->
            notify(
                userId = userId,
                source = source,
                from = from,
                subject = subject,
                contentId = contentId,
                timestamp = timestamp
            )
        }
    }

    private fun notify(
        userId: Long,
        source: Source,
        from: String,
        subject: String,
        contentId: Long,
        timestamp: DateTime
    ) {
        val inbox: Inbox = resolveInbox(userId, source)
        val index: Int = listMessages(userId).maxBy { message ->
            message.index
        }?.index ?: 0

        messages = messages + Message.new(
            inboxId = inbox.id,
            index = index + 1,
            from = from,
            subject = subject,
            contentId = contentId,
            tags = listOf(INBOX_TAG),
            timestamp = timestamp
        )
    }

    private fun resolveInbox(
        userId: Long,
        source: Source,
        defaultValue: Inbox? = null
    ): Inbox {
        return inboxes.find { inbox ->
            inbox.userId == userId &&
            inbox.source == source
        } ?: defaultValue?.let {
            inboxes = inboxes + defaultValue
            defaultValue
        } ?: throw RuntimeException("Cannot resolve inbox for user/source: $userId/$source")
    }

    private fun listMessages(inboxId: UUID): List<Message> {
        return messages.filter { message ->
            message.inboxId == inboxId
        }
    }
}
