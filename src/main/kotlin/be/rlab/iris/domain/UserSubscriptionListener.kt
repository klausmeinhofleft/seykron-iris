package be.rlab.iris.domain

import be.rlab.iris.domain.model.Source
import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.MessageListener
import be.rlab.tehanu.domain.model.ChatEvent
import be.rlab.tehanu.domain.model.ChatMessage
import be.rlab.tehanu.domain.model.Permission
import me.ivmg.telegram.entities.User as ChatUser

class UserSubscriptionListener(
    private val accessControl: AccessControl,
    private val inboxService: InboxService
) : MessageListener<ChatMessage> {

    override fun handle(
        context: MessageContext,
        message: ChatMessage
    ): MessageContext {
        val event: ChatEvent = message.event
        val userId: Long = (message.data as ChatUser).id

        if (event.addChatMember || event.leftChatMember) {
            accessControl.findUserById(userId)?.let { user ->
                val hasPermissions: Boolean = accessControl.checkPermissions(
                    context.chat, user, Permission.SUBSCRIPTIONS
                )

                when {
                    event.addChatMember && hasPermissions -> inboxService.subscribe(
                        userId = user.id,
                        source = Source.from(context.chat)
                    )
                    event.leftChatMember -> inboxService.unsubscribe(
                        userId = user.id,
                        source = Source.from(context.chat)
                    )
                }
            }
        }

        return context.done()
    }
}
