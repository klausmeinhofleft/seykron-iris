package be.rlab.iris.domain.command

import be.rlab.iris.domain.InboxService
import be.rlab.iris.domain.model.Message
import be.rlab.iris.domain.model.Source
import be.rlab.tehanu.domain.AccessControl
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class InboxRead(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val inboxService: InboxService,
    private val accessControl: AccessControl
) : Command {

    private val logger: Logger = LoggerFactory.getLogger(InboxRead::class.java)

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        return if (context.retries == 0) {
            logger.info("Displaying messages to read")
            showMessages(context)
        } else {
            logger.info("Reading message: ${message.text}")
            readMessage(context, message.text)
        }
    }

    private fun showMessages(context: MessageContext): MessageContext {
        val messages: Map<Source, List<Message>> = inboxService.listMessages(context.user.id) { inbox, messages ->
            inbox.source to messages
        }.filter { (source, messages) ->
            messages.isNotEmpty() &&
            (context.chat.type == ChatType.PRIVATE ||
                Source.from(context.chat) == source)
        }.toMap()

        return if (messages.isNotEmpty()) {
            val summary: String = messages.map { (source, messages) ->
                source to messages.joinToString(separator = "\n") { message ->
                    "\\[${message.index}] ${message.subject}"
                }
            }.joinToString(separator = "\n") { (source, summary) ->
                "# ${source.name}\n$summary"
            }

            context.talk("¿Qué número de mensaje querés que lea?\n\n$summary")
            context.retry()
        } else {
            context.answer("No tenés mensajes para leer")
        }
    }

    private fun readMessage(
        context: MessageContext,
        text: String
    ): MessageContext {
        return context.parseInput(text, """
            Seleccioná uno de los números de mensaje.
        """.trimIndent()) { index: Int ->
            inboxService.getMessage(context.user.id, index)?.let { message ->
                val content: String = inboxService.readContent(message).data.bufferedReader().readText()

                if (context.chat.type == ChatType.PRIVATE) {
                    context.talk(
                        "From: ${message.from}" +
                        "\nSubject: ${message.subject}" +
                        "\nSend Date: ${message.timestamp}" +
                        "\nContent:\n\n$content")
                } else {
                    notifyAdmins(context, message)
                    context.talk(
                        "Subject: ${message.subject}" +
                        "\nSend Date: ${message.timestamp}" +
                        "\nContent:\n\n$content")
                }
            } ?: let {
                context.talk("El mensaje no existe, elegí un número de mensaje válido")
            }
        }
    }

    private fun notifyAdmins(
        context: MessageContext,
        message: Message
    ) {

        accessControl.getAdminsIds(context.chat).forEach { adminId ->
            accessControl.findChat(adminId)?.let { chat ->
                context.talkTo(chat, """
                    @${context.user.userName} leyó un mensaje de '${message.from}' en el chat '${context.chat.title}'.
                    Subject: ${message.subject}
                """.trimIndent())
            }
        }
    }
}
