package be.rlab.iris.domain.command

import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import be.rlab.iris.domain.model.DefaultSources

class ListSources(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long
) : Command {

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        val sourcesMessage: String = DefaultSources.values().filter { source ->
            source.allowSubscription
        }.joinToString(separator = "\n") { source ->
            "${source.eventType} - ${source.description}"
        }
        return context.talk(sourcesMessage)
    }
}
