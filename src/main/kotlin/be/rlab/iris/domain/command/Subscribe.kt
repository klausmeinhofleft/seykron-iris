package be.rlab.iris.domain.command

import be.rlab.iris.domain.InboxService
import be.rlab.iris.domain.model.DefaultSources
import be.rlab.iris.domain.model.Source
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage

class Subscribe(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val inboxService: InboxService
) : Command {

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        return when (context.chat.type) {
            ChatType.GROUP -> subscribeGroup(context)
            ChatType.PRIVATE -> subscribeNotificationChannel(context, message.text)
            else -> throw RuntimeException("Unsupported chat")
        }
    }

    private fun subscribeGroup(context: MessageContext): MessageContext {
        inboxService.subscribe(
            userId = context.user.id,
            source = Source.from(context.chat)
        )
        return context.answer("te subscribí a las notificaciones de este grupo")
    }

    private fun subscribeNotificationChannel(
        context: MessageContext,
        text: String
    ): MessageContext {
        return if (context.retries == 0) {
            val defaultSources: List<DefaultSources> = DefaultSources.values().filter { source ->
                source.allowSubscription && !inboxService.isSubscribed(
                    userId = context.user.id,
                    source = source.toSource()
                )
            }

            if (defaultSources.isEmpty()) {
                context.talk("No hay orígenes de datos para subscribirte")
            } else {
                val sourcesMessage: String = defaultSources.joinToString(separator = "\n") { source ->
                    "${source.eventType} - ${source.description}"
                }
                context.talk("A qué origen de datos querés subscribirte?\n\n$sourcesMessage")
                context.retry()
            }
        } else {
            context.parseInput(text,
                "Elegí uno de los orígenes de datos listados arriba"
            ) { sourceInput: DefaultSources ->
                inboxService.subscribe(
                    userId = context.user.id,
                    source = Source(
                        id = sourceInput.name,
                        name = sourceInput.eventType
                    )
                )
                context.talk("Listo! quedaste subscripta")
            }
        }
    }
}
