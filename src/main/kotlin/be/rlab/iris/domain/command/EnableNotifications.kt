package be.rlab.iris.domain.command

import be.rlab.iris.domain.MemorySlots.CHANNELS_SUBSCRIPTIONS
import be.rlab.iris.domain.model.DefaultSources
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage
import java.util.*

class EnableNotifications(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    memory: Memory
) : Command {

    private var channelSubscriptions: Map<UUID, List<DefaultSources>> by memory.slot(CHANNELS_SUBSCRIPTIONS, mapOf<UUID, List<DefaultSources>>())

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        val subscriptions: List<DefaultSources> = channelSubscriptions[context.chat.id] ?: emptyList()

        return if (context.retries == 0) {
            val defaultSources: List<DefaultSources> = DefaultSources.values().filter { source ->
                source.allowSubscription && !subscriptions.contains(source)
            }

            if (defaultSources.isEmpty()) {
                context.answer("no hay orígenes de datos para subscribirse")
            } else {
                val sourcesMessage: String = defaultSources.joinToString(separator = "\n") { source ->
                    "${source.eventType} - ${source.description}"
                }
                context.answer("de qué origen de datos querés que el grupo reciba notificaciones?\n\n$sourcesMessage")
                context.retry()
            }
        } else {
            context.parseInput(message.text,
                "Elegí uno de los orígenes de datos listados arriba"
            ) { sourceInput: DefaultSources ->
                channelSubscriptions = channelSubscriptions + (context.chat.id to subscriptions + sourceInput)
                context.talk("Listo! este grupo va a recibir las notificaciones del origen de datos")
            }
        }
    }
}
