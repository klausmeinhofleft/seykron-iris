package be.rlab.iris.domain.command

import be.rlab.iris.domain.InboxService
import be.rlab.iris.domain.model.Source
import be.rlab.tehanu.domain.Command
import be.rlab.tehanu.domain.MessageContext
import be.rlab.tehanu.domain.model.ChatType
import be.rlab.tehanu.domain.model.TextMessage

class InboxCount(
    override val name: String,
    override val scope: List<ChatType>,
    override val timeout: Long,
    private val inboxService: InboxService
) : Command {

    override fun handle(
        context: MessageContext,
        message: TextMessage
    ): MessageContext {
        val count: Map<Source, Int> = inboxService.count(context.user.id)

        return when (context.chat.type) {
            ChatType.GROUP -> {
                val chatCount: Int = count[Source.from(context.chat)] ?: 0
                if (chatCount == 0) {
                    context.answer("no tengo mensajes pendientes de este chat para vos")
                } else {
                    context.answer("tenés $chatCount mensaje(s) pendiente(s)")
                }
            }
            ChatType.PRIVATE -> {
                if (count.isNotEmpty()) {
                    val answer: String = count.entries.joinToString(separator = "\n") { (source, count) ->
                        "${source.name}: $count mensaje(s)"
                    }
                    context.talk(answer)
                } else {
                    context.answer("no tengo mensajes pendientes para vos")
                }
            }
            else ->
                context.answer("no tengo mensajes pendientes para vos")
        }
    }
}
