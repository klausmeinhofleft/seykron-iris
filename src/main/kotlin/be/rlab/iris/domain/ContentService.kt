package be.rlab.iris.domain

import be.rlab.tehanu.domain.Memory
import be.rlab.iris.domain.MemorySlots.CONTENT_SLOT
import be.rlab.iris.domain.model.Content
import be.rlab.iris.domain.model.ContentRecord
import net.openhft.hashing.LongHashFunction
import org.springframework.util.Base64Utils
import java.io.ByteArrayOutputStream
import java.io.InputStream
import java.util.zip.GZIPInputStream
import java.util.zip.GZIPOutputStream

class ContentService(memory: Memory) {

    private var contents: Map<Long, ContentRecord> by memory.slot(CONTENT_SLOT, mapOf<Long, ContentRecord>())

    fun store(content: Content): Long {
        val data: String = serialize(content.data)
        val contentId: Long = contentId(data)

        return if (contents.containsKey(contentId)) {
            contentId
        } else {
            val record = ContentRecord.new(
                contentId = contentId,
                contentType = content.contentType,
                encoding = content.encoding,
                data = data
            )
            contents = contents + (contentId to record)
            contentId
        }
    }

    fun retrieve(contentId: Long): Content? {
        return contents[contentId]?.let { record ->
            Content(
                contentType = record.contentType,
                encoding = record.encoding,
                data = deserialize(record.data)
            )
        }
    }

    private fun contentId(data: String): Long =
        LongHashFunction.xx().hashBytes(data.toByteArray())

    private fun serialize(data: InputStream): String {
        val out = ByteArrayOutputStream()

        GZIPOutputStream(out).use { gzip ->
            gzip.write(data.readBytes())
        }

        return Base64Utils.encodeToString(out.toByteArray())
    }

    private fun deserialize(
        data: String
    ): InputStream {
        return GZIPInputStream(
            Base64Utils.decodeFromString(data).inputStream()
        )
    }
}
