package be.rlab.iris.domain.model

enum class Flag {
    SEEN,
    UNSEEN,
    ANSWERED,
    DRAFT,
    DELETED,
    FLAGGED
}
