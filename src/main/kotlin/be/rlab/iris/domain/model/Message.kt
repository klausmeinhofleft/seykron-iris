package be.rlab.iris.domain.model

import org.joda.time.DateTime
import java.util.*

data class Message(
    val inboxId: UUID,
    val messageId: UUID,
    val index: Int,
    val from: String,
    val flags: List<Flag>,
    val subject: String,
    val contentId: Long,
    val tags: List<String>,
    val timestamp: DateTime = DateTime.now()
) {
    companion object {
        fun new(
            inboxId: UUID,
            index: Int,
            from: String,
            subject: String,
            contentId: Long,
            tags: List<String>,
            timestamp: DateTime
        ): Message = Message(
            inboxId = inboxId,
            messageId = UUID.randomUUID(),
            index = index,
            flags = listOf(Flag.UNSEEN),
            from = from,
            subject = subject,
            contentId = contentId,
            tags = tags,
            timestamp = timestamp
        )
    }
}
