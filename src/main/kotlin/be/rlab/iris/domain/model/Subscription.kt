package be.rlab.iris.domain.model

import java.util.*

data class Subscription(
    val inboxId: UUID,
    val source: Source
) {
    companion object {
        fun new(
            inboxId: UUID,
            source: Source
        ): Subscription = Subscription(
            inboxId = inboxId,
            source = source
        )
    }
}
