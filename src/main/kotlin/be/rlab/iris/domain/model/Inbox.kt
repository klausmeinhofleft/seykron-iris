package be.rlab.iris.domain.model

import java.util.*

data class Inbox(
    val id: UUID,
    val userId: Long,
    val source: Source
) {
    companion object {
        fun new(
            userId: Long,
            source: Source
        ): Inbox = Inbox(
            id = UUID.randomUUID(),
            userId = userId,
            source = source
        )
    }
}
