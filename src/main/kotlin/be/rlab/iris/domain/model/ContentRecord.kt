package be.rlab.iris.domain.model

import java.nio.charset.Charset

data class ContentRecord(
    val version: Int,
    val contentId: Long,
    val contentType: String,
    val encoding: Charset,
    val data: String,
    val deleted: Boolean
) {
    companion object {
        const val CURRENT_VERSION: Int = 1

        fun new(
            contentId: Long,
            contentType: String,
            encoding: Charset,
            data: String
        ): ContentRecord = ContentRecord(
            version = CURRENT_VERSION,
            contentId = contentId,
            contentType = contentType,
            encoding = encoding,
            data = data,
            deleted = false
        )
    }
}
