package be.rlab.iris.domain.model

import java.io.InputStream
import java.nio.charset.Charset

data class Content(
    val contentType: String,
    val encoding: Charset = Charset.defaultCharset(),
    val data: InputStream
) {
    companion object {
        fun text(content: String): Content =
            Content(
                contentType = "text/plain",
                data = content.byteInputStream()
            )

        fun markdown(content: String): Content =
            Content(
                contentType = "text/markdown",
                data = content.byteInputStream()
            )

        fun yaml(content: String): Content =
            Content(
                contentType = "text/yaml",
                data = content.byteInputStream()
            )

        fun json(content: String): Content =
            Content(
                contentType = "application/json",
                data = content.byteInputStream()
            )
    }
}
