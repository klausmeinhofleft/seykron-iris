package be.rlab.iris.domain.model

enum class DefaultSources(
    val eventType: String,
    val description: String,
    val allowSubscription: Boolean
) {
    CONNECT("CONNECT", "Receives notifications when a user access a server via SSH", allowSubscription = true),
    SERVICE_STATUS("SERVICE_STATUS", "Infrastructure services status report", allowSubscription = true),
    UNKNOWN("UNKNOWN", "Unknown source", allowSubscription = false);

    fun toSource(): Source {
        return Source(
            id = name,
            name = eventType
        )
    }

    companion object {
        fun fromEventType(eventType: String): DefaultSources {
            return values().find { source ->
                source.eventType == eventType
            } ?: UNKNOWN
        }
    }
}
