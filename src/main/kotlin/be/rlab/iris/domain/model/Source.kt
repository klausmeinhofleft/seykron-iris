package be.rlab.iris.domain.model

import be.rlab.tehanu.domain.model.Chat

data class Source(
    val id: String,
    val name: String
) {
    companion object {
        fun from(chat: Chat): Source = Source(
            id = chat.id.toString(),
            name = chat.title ?: ""
        )
    }
}
