package be.rlab.iris.domain.model

import org.joda.time.DateTime

data class Notification(
    val userName: String,
    val subject: String,
    val summary: String?,
    val timestamp: DateTime,
    val eventType: String,
    val additionalData: Map<String, String>,
    val flags: List<Flag>
) {
    companion object {
        fun new(
            userName: String,
            subject: String,
            summary: String,
            eventType: String,
            additionalData: Map<String, String>
        ): Notification = Notification(
            userName = userName,
            subject = subject,
            summary = summary,
            timestamp = DateTime.now(),
            eventType = eventType,
            additionalData = additionalData,
            flags = listOf(Flag.UNSEEN)
        )
    }

    fun markAsSeen(): Notification {
        return copy(
            flags = flags.filterNot { flag ->
                flag == Flag.UNSEEN
            } + Flag.SEEN
        )
    }
}
