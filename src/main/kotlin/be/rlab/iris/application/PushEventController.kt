package be.rlab.iris.application

import be.rlab.iris.application.model.NotificationDTO
import be.rlab.iris.domain.NotificationService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/eoc")
class PushEventController(
    private val notificationService: NotificationService
) {

    @PostMapping("/notifications")
    @ResponseStatus(HttpStatus.ACCEPTED)
    fun pushNotification(
        @RequestBody notification: NotificationDTO
    ) {
        notificationService.push(notification.asNotification())
    }
}
