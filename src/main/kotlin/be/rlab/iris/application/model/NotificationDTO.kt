package be.rlab.iris.application.model

import be.rlab.iris.domain.model.Notification

data class NotificationDTO(
    val userName: String,
    val subject: String,
    val summary: String,
    val eventType: String,
    val additionalData: Map<String, String>
) {

    fun asNotification(): Notification {
        return Notification.new(
            userName = userName,
            subject = subject,
            summary = summary,
            eventType = eventType,
            additionalData = additionalData
        )
    }
}
