package be.rlab.iris

import be.rlab.iris.config.CommandBeans
import be.rlab.iris.config.DomainBeans
import be.rlab.iris.config.WebConfig
import be.rlab.iris.domain.NotificationService
import be.rlab.tehanu.config.BotBeans
import be.rlab.tehanu.config.DataSourceBeans
import be.rlab.tehanu.config.MemoryBeans
import be.rlab.tehanu.domain.Tehanu
import be.rlab.tehanu.util.persistence.DataSourceInitializer
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.slf4j.bridge.SLF4JBridgeHandler
import org.springframework.beans.factory.getBean
import org.springframework.context.annotation.AnnotationConfigApplicationContext
import org.springframework.http.server.reactive.HttpHandler
import org.springframework.http.server.reactive.ReactorHttpHandlerAdapter
import org.springframework.web.server.adapter.WebHttpHandlerBuilder
import reactor.netty.http.server.HttpServer

class Application(port: Int = 8090) {

    private val logger: Logger = LoggerFactory.getLogger(Application::class.java)

    private val httpHandler: HttpHandler

    init {
        val context = AnnotationConfigApplicationContext {
            DataSourceBeans.beans().initialize(this)
            MemoryBeans.beans().initialize(this)
            DomainBeans.beans().initialize(this)
            BotBeans.beans().initialize(this)
            CommandBeans.beans().initialize(this)
            register(WebConfig::class.java)
            refresh()
        }

        logger.info("Initializing data source")
        val dataSourceInitializer: DataSourceInitializer = context.getBean()
        dataSourceInitializer.init()

        logger.info("Starting notification polling")
        val notificationService: NotificationService = context.getBean()
        notificationService.poll()

        httpHandler = WebHttpHandlerBuilder
            .applicationContext(context)
            .build()

        HttpServer.create()
            .port(port)
            .handle(ReactorHttpHandlerAdapter(httpHandler))
            .bindNow()
        logger.info("Application started")

        val tehanu: Tehanu = context.getBean()
        logger.info("Iris is entering Telegram")
        tehanu.start()
    }
}

fun main(args: Array<String>) {
    // Sets up jul-to-slf4j bridge. I have not idea
    // why the logging.properties strategy doesn't work.
    SLF4JBridgeHandler.removeHandlersForRootLogger()
    SLF4JBridgeHandler.install()

    Application()
}
