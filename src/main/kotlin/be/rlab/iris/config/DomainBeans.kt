package be.rlab.iris.config

import be.rlab.iris.domain.ContentService
import be.rlab.iris.domain.InboxService
import be.rlab.iris.domain.NotificationService
import be.rlab.iris.domain.UserSubscriptionListener
import be.rlab.iris.util.ObjectMapperFactory
import be.rlab.tehanu.domain.MessageListenerDefinition
import be.rlab.tehanu.domain.model.ChatMessage
import org.springframework.context.support.beans
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter

object DomainBeans {
    fun beans() = beans {
        // Services
        bean<InboxService>()
        bean<ContentService>()
        bean<NotificationService>()
        bean {
            MessageListenerDefinition(
                messageType = ChatMessage::class,
                listener = UserSubscriptionListener(
                    accessControl = ref(),
                    inboxService = ref()
                )
            )
        }

        bean {
            MappingJackson2HttpMessageConverter(ObjectMapperFactory.snakeCaseMapper)
        }
    }
}