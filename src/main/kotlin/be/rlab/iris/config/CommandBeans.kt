package be.rlab.iris.config

import be.rlab.iris.domain.command.*
import be.rlab.tehanu.domain.model.ChatType
import org.springframework.context.support.beans

object CommandBeans {
    fun beans() = beans {
        bean {
            ListSources(
                name = "/list_sources",
                scope = listOf(ChatType.PRIVATE),
                timeout = -1
            )
        }
        bean {
            Subscribe(
                name = "/subscribe",
                scope = listOf(ChatType.PRIVATE, ChatType.GROUP),
                timeout = 60000,
                inboxService = ref()
            )
        }
        bean {
            EnableNotifications(
                name = "/enable_notifications",
                scope = listOf(ChatType.GROUP),
                timeout = 60000,
                memory = ref()
            )
        }
        bean {
            InboxRead(
                name = "/inbox_read",
                scope = listOf(ChatType.PRIVATE, ChatType.GROUP),
                timeout = 60000,
                inboxService = ref(),
                accessControl = ref()
            )
        }
        bean {
            InboxCount(
                name = "/inbox_count",
                scope = listOf(ChatType.PRIVATE, ChatType.GROUP),
                timeout = -1,
                inboxService = ref()
            )
        }
    }
}
