package be.rlab.tehanu.domain

import be.rlab.tehanu.domain.model.Chat
import be.rlab.tehanu.domain.model.User
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import me.ivmg.telegram.entities.Message
import java.util.*

class TestAccessControl {
    val instance: AccessControl = mock()

    fun addChatIfRequired(
        message: Message,
        result: Chat
    ): TestAccessControl {
        whenever(instance.addChatIfRequired(message)).thenReturn(result)
        return this
    }

    fun addUserIfRequired(
        message: Message,
        result: User
    ): TestAccessControl {
        whenever(instance.addUserIfRequired(message)).thenReturn(result)
        return this
    }

    fun findChatById(
        chatId: UUID,
        result: Chat
    ): TestAccessControl {
        whenever(instance.findChatById(chatId)).thenReturn(result)
        return this
    }

    fun checkPermissions(
        command: Command,
        chat: Chat,
        user: User,
        result: Boolean
    ): TestAccessControl {
        whenever(instance.checkPermissions(command, chat, user)).thenReturn(result)
        return this
    }

    fun listUsers(
        chatId: UUID,
        result: List<User>
    ): TestAccessControl {
        whenever(instance.listUsers(chatId)).thenReturn(result)
        return this
    }

    fun findUserById(
        userId: Long,
        result: User
    ): TestAccessControl {
        whenever(instance.findUserById(userId)).thenReturn(result)
        return this
    }
}