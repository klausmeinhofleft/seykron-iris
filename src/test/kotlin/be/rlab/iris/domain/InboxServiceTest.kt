package be.rlab.iris.domain

import be.rlab.iris.domain.MemorySlots.SUBSCRIPTIONS_SLOT
import be.rlab.iris.domain.model.Content
import be.rlab.iris.domain.model.Source
import be.rlab.iris.domain.model.Subscription
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.TestAccessControl
import be.rlab.tehanu.domain.TestMemory
import be.rlab.tehanu.domain.model.Chat
import be.rlab.tehanu.domain.model.TestChat
import be.rlab.tehanu.domain.model.TestUser
import be.rlab.tehanu.domain.model.User
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.whenever
import org.joda.time.DateTime
import org.junit.Test
import java.util.*

class InboxServiceTest {

    private val memory: Memory = TestMemory()
    private val subscriptions: Map<Long, List<Subscription>> by memory.slot(SUBSCRIPTIONS_SLOT, mapOf<Long, List<Subscription>>())

    private val contentService: ContentService = mock()
    private val accessControl: TestAccessControl = TestAccessControl()
    private val inboxService: InboxService = InboxService(
        memory = memory,
        contentService = contentService,
        accessControl = accessControl.instance
    )

    private val userId: Long = 1234
    private val source: Source = Source(
        id = "test-source",
        name = "test source"
    )

    @Test
    fun subscribe() {
        assert(subscriptions.isEmpty())
        inboxService.subscribe(userId, source)
        assert(subscriptions[userId] != null)
    }

    @Test
    fun unsubscribe() {
        assert(subscriptions.isEmpty())
        inboxService.subscribe(userId, source)
        inboxService.unsubscribe(userId, source)
        assert(subscriptions[userId]?.isEmpty() ?: false)
    }

    @Test
    fun broadcast_chat() {
        val content: Content = mock()
        whenever(contentService.store(content)).thenReturn(1234)

        val chatId: UUID = UUID.randomUUID()
        val chat: Chat = TestChat(
            id = chatId,
            title = "test-source"
        ).new()

        accessControl
            .findChatById(chatId, chat)
            .listUsers(chatId, listOf(TestUser(id = userId).new()))

        inboxService.subscribe(userId, Source.from(chat))
        inboxService.broadcast(chatId, "test@foo", "test subject", content, DateTime.now())

        assert(inboxService.listMessages(userId).size == 1)
    }

    @Test
    fun broadcast_source() {
        val content: Content = mock()
        whenever(contentService.store(content)).thenReturn(1234)

        val chatId: UUID = UUID.randomUUID()
        val chat: Chat = TestChat(
            id = chatId,
            title = "test-source"
        ).new()

        accessControl
            .findChatById(chatId, chat)
            .listUsers(chatId, listOf(TestUser(id = userId).new()))

        val source = Source(
            id = "test-source",
            name = "test source title"
        )
        inboxService.subscribe(userId, source)
        inboxService.broadcast(source, "test@foo", "test subject", content, DateTime.now())

        assert(inboxService.listMessages(userId).size == 1)
    }

    @Test
    fun listSubscriptors() {
        val user: User = TestUser(id = userId).new()
        val source = Source(
            id = "test-source",
            name = "test source title"
        )
        accessControl.findUserById(userId, user)
        inboxService.subscribe(userId, source)
        val subscriptors: List<User> = inboxService.listSubscriptors(source)
        assert(subscriptors.size == 1)
        assert(subscriptors.contains(user))
    }
}