package be.rlab.iris.domain

import be.rlab.iris.domain.MemorySlots.CONTENT_SLOT
import be.rlab.iris.domain.model.Content
import be.rlab.iris.domain.model.ContentRecord
import be.rlab.tehanu.domain.Memory
import be.rlab.tehanu.domain.TestMemory
import org.junit.Test

class ContentServiceTest {

    private val memory: Memory = TestMemory()
    private val contentService: ContentService =
        ContentService(memory)
    private val contents: Map<Long, ContentRecord> by memory.slot(CONTENT_SLOT, mapOf<Long, ContentRecord>())

    @Test
    fun store() {
        val content: Content = Content.text("hello world")
        val contentId: Long = contentService.store(content)

        assert(contents[contentId] != null)
    }

    @Test
    fun retrieve() {
        val content: Content = Content.text("hello world")
        val contentId: Long = contentService.store(content)

        contentService.retrieve(contentId)?.let { storedContent ->
            assert(storedContent.contentType == content.contentType)
            assert(storedContent.encoding == content.encoding)
            content.data.reset()
            assert(storedContent.data.bufferedReader().readText() == content.data.bufferedReader().readText())
        }
    }
}
